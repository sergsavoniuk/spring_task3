package com.springmvc.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.persistence.criteria.Order;
import java.util.List;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "car_id")
    private long car_id;

    @NotEmpty
    @Column(name = "makes", nullable = false)
    private String makes;

    @NotEmpty
    @Column(name = "model", nullable = false)
    private String model;

    @NotEmpty
    @Column(name = "engine", nullable = false)
    private String engine;

    @NotEmpty
    @Column(name = "mileage", nullable = false)
    private double mileage;

    @NotEmpty
    @Column(name = "year", nullable = false)
    private int year;

    /*@ManyToOne
    @JoinColumn(name = "login")
    private User user;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "order_car", joinColumns = @JoinColumn(name = "car_id"),
                inverseJoinColumns = @JoinColumn(name = "order_id"))
    private List<Order> orders;
*/
    public Car() {
    }

    public Car(String makes, String model, String engine, double mileage, int year) {
        this.makes = makes;
        this.model = model;
        this.engine = engine;
        this.mileage = mileage;
        this.year = year;
    }

    /*public Car(String makes, String model, String engine, double mileage, int year, User user, List<Order> orders) {
        this.makes = makes;
        this.model = model;
        this.engine = engine;
        this.mileage = mileage;
        this.year = year;
        this.user = user;
        this.orders = orders;
    }*/

    public long getId() {
        return car_id;
    }

    public void setId(long car_id) {
        this.car_id = car_id;
    }

    public String getMakes() {
        return makes;
    }

    public void setMakes(String makes) {
        this.makes = makes;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

   /* public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }*/
}
